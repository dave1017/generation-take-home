var request = require('request')
var storesJSON = require('./store_directory.json')
var async = require('async')
var fs = require('fs')

var stores = []
console.log('TOTAL DE REGISTROS: ', stores.length)
async.mapLimit(storesJSON, 1, function(x, callback) {
    var url = {
        url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + x.Address,
        method: 'get',
        json: true
    }
    console.log(JSON.stringify(url, null, 4))
    request(url, function(err, httpResponse, body) {
        if(err){
            stores.push({
                "Name": x.Name,
                "Address": x.Address
            })
            callback(null, true)
        } else {
            if (body.results[0]) {
                if (body.results[0].geometry) {
                    if (body.results[0].geometry.location) {
                        let lat = body.results[0].geometry.location.lat
                        let lng = body.results[0].geometry.location.lng
                        stores.push({
                            "Name": x.Name,
                            "Address": x.Address,
                            "lat": lat,
                            "lng": lng
                        })
                    } else {
                        stores.push({
                            "Name": x.Name,
                            "Address": x.Address
                        })
                    }
                } else {
                    stores.push({
                        "Name": x.Name,
                        "Address": x.Address
                    })
                }
            } else {
                stores.push({
                    "Name": x.Name,
                    "Address": x.Address
                })
            }
            callback(null, true)
        }
    })
},
function(err, res) {
    fs.writeFileSync('./store_directory2.json', JSON.stringify(stores, null, 4))
    console.log('completado')
})
