import React, { Component } from 'react';
import { Map, Marker, InfoWindow, GoogleApiWrapper } from 'google-maps-react';
import axios from 'axios'

// export default class MapContainer extends Component {
class MapContainer extends Component {
    constructor() {
        super();
        this.state = {
            marks: [],
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
            notiStatus: false,
            notiStatusDel: false,
            listMarks: []
        };
        this.marks = [];
        this.count = 0;
    }

    componentDidMount() {
        let marks = []
        axios.get('../store_directory2.json') // JSON File Path
            .then(response => {
                marks = response.data
                this.setState({
                    marks: marks
                });
                // for (let i = 0; i < marks.length; i++) {
                //     axios.get('http://maps.googleapis.com/maps/api/geocode/json?address=' + marks[i].Address)
                //         .then(res => {
                //             let persons = res.data;
                //             if (persons.results[0]) {
                //                 if (persons.results[0].geometry) {
                //                     if (persons.results[0].geometry.location) {
                //                         marks[i].lat = persons.results[0].geometry.location.lat
                //                         marks[i].lng = persons.results[0].geometry.location.lng
                //                     }
                //                 }
                //             }
                //             if (i == (marks.length - 1)) {
                //                 console.log("TOTAL DE REGISTROS: ", marks.length)
                //                 this.setState({
                //                     marks: marks
                //                 });
                //             }
                //         })
                // }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true,
            notiStatus: !this.state.notiStatus
        });

        this.state.listMarks[this.count] = {
            Name: this.state.selectedPlace.title,
            Address: this.state.selectedPlace.name
        }
        this.count++
        this.setState({ listMarks: this.state.listMarks });

        console.log("onMarkerClick")
        console.log("showingInfoWindow", this.state.showingInfoWindow)
        console.log("activeMarker", this.state.activeMarker)
        console.log("selectedPlace", this.state.selectedPlace)
        console.log("notiStatus", this.state.notiStatus)
        console.log("listMarks", this.state.listMarks)

        setTimeout(() => {
            this.setState({ notiStatus: !this.state.notiStatus });
        }, 1000);
    };

    onMapClick = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
        console.log("onMapClick")
        console.log("showingInfoWindow", this.state.showingInfoWindow)
        console.log("activeMarker", this.state.activeMarker)
    };

    removeFavorite = (id) => {
        // update the state object
        delete this.state.listMarks[id];
        // set the state
        this.setState({ 
            listMarks: this.state.listMarks,
            notiStatusDel: !this.state.notiStatusDel
        });

        setTimeout(() => {
            this.setState({ notiStatusDel: !this.state.notiStatusDel });
        }, 1000);
    }

    render() {
        return (
            <div>
                <WorkMarkers removeFavorite={this.removeFavorite} listMarks={this.state.listMarks}/>
                {
                    this.state.notiStatus?<MessageAdd />:null
                }
                {
                    this.state.notiStatusDel?<MessageDel />:null
                }
                <Map
                    google={this.props.google}
                    style={{ width: '100%', height: '100%' }}
                    initialCenter={{ lat: 19.4326, lng: -99.1332 }}
                    zoom={12}
                    onClick={this.onMapClick}
                >

                    {this.state.marks.map((item, i) =>
                        <Marker
                            key={i}
                            title={item.Name}
                            name={item.Address}
                            position={{ lat: item.lat, lng: item.lng }}
                            onClick={this.onMarkerClick}
                        />
                    )}
                    {/* {this.state.marks.map((item, i) =>
                        <InfoWindow key={i} marker={this.state.activeMarker} visible={this.state.showingInfoWindow}>
                            <div style={{ width: '250px', height: '170px' }}>
                                <h6>{item.Name}</h6>
                                <p>{item.Address}</p>
                            </div>
                        </InfoWindow>
                    )} */}
                </Map>
            </div>
		);
    }
}

class WorkMarkers extends Component{
    openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
        // document.body.style.backgroundColor = "white";
    }

    deleteMarker(e) {
        console.log('ELIMINAR ELEMENTO: ', e)
        this.props.removeFavorite(e);
    }

    render() {
        return (
            <div>
                <div id="mySidenav" className="sidenav">
                    <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav.bind(this)}>&times;</a>
                    <a href="">My Favorite Stores</a>
                    {/* <a ><div><span style={{ fontSize: '20px', color: 'red' }}>&times;</span> ITEM</div></a> */}
                    {this.props.listMarks.map((item, i) =>
                        <a key={i.toString()} value={i}><div onClick={this.deleteMarker.bind(this, i)}><span style={{ fontSize: '20px', color: 'red' }}>&nbsp;&nbsp;  &times;</span>{item.Name}</div></a>
                    )}
                </div>

                <div id="main">
                    <span style={{ fontSize: '30px', cursor: 'pointer' }} onClick={this.openNav.bind(this)}>&#9776;</span>
                    <span style={{ paddingLeft: '30px' }}>Select your Favorites Stores</span>
                </div>
            </div>
        );
    }
}

let styleAdd = { 
    width: '200px', 
    height: '50px', 
    backgroundColor: 'green', 
    color: 'white', 
    padding: '12px', 
    position: 'fixed', 
    right: '0', 
    top: '15px'
}

class MessageAdd extends Component {
    render() {
        return (
            <div>
                <div id="message" style={styleAdd}>
                    <span>Store added to favorites</span>
                </div>
            </div>
        );
    }
}

let styleDel = { 
    width: '225px', 
    height: '50px', 
    backgroundColor: 'red', 
    color: 'white', 
    padding: '12px', 
    position: 'fixed', 
    right: '0', 
    top: '15px',
    zIndex: '30'
}

class MessageDel extends Component {
    render() {
        return (
            <div>
                <div id="message" style={styleDel}>
                    <span>Store deleted from favorites</span>
                </div>
            </div>
        );
    }
}


export default GoogleApiWrapper({
    apiKey: 'AIzaSyDK8JO_RyOmldyIT8ohkvZz_XOGnzmmT9E',
})(MapContainer)